# Wurmpy-Guru
A timer app for meditating in the game [Wurm Online](https://wurmonline.com)

# Todo:
- [ ] functional timers
- [ ] cross-platform data caching
- [ ] automatic uptime fetching from https://harmony.game.wurmonline.com/battles/stats.html or https://harmony.game.wurmonline.com/battles/stats.xml
